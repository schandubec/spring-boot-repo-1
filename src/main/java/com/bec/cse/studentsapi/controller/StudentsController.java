package com.bec.cse.studentsapi.controller;

import com.bec.cse.studentsapi.domain.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/test/students")
public class StudentsController {

    List<Student> students;

    public StudentsController() {
        students = new ArrayList<>();
        students.add(new Student("1","Chandra","Sekhar"));
        students.add(new Student("2","Kishan","Chand"));
        students.add(new Student("3","Chandra","Sekhar"));
        students.add(new Student("4","Kishan","Chand"));
    }

    //@GetMapping(value="/id")
    @RequestMapping(value="/id",method = RequestMethod.GET)
    public String getStudent1(){
        return "Hello";
    }

    @GetMapping()
    public List<Student> getStudentsList(){
        return students;
    }

    @GetMapping(value="/{id}")
    public Student getStudent(@PathVariable String id){

        //return students.get(id);

        /*
        Optional<Student> student = students.stream()
                                            .filter(x->x.getStudentId().equalsIgnoreCase(id))
                                            .findFirst();
        return student.get();
        */


        Student student=null;
        for(int i=0;i<students.size();i++){
            if(students.get(i).getStudentId().equalsIgnoreCase(id)){
                student = students.get(i);
            }
        }
        return student;

    }

    @PostMapping
    public List<Student> getStudents(@RequestBody Student student){

        students.add(student);
        return students;
    }

    @PutMapping
    public void editStudents(@RequestBody Student student){
        Student student1 = getStudent(student.getStudentId());
        //student1.setFirstName(student.getFirstName());
        student1.setLastName(student.getLastName());
        //return "";
    }

    @DeleteMapping(value="/{id}")
    public void editStudents(@PathVariable String id){
        students.remove(getStudent(id));
    }

}
